import {
    calculateBonuses
} from "./bonus-system.js";
const assert = require("assert");

describe('Bonus system tests', () => {
    test('Valid program and valid amount', () => {
        expect(calculateBonuses("Standard", 5000)).toEqual(0.05);
    });

    test('Valid program and boundary amount (10000)', (done) => {
        assert.equal(calculateBonuses("Diamond", 10000), 0.2 * 1.5);
        done();
    });

    test('Valid program and boundary amount (50000)', (done) => {
        assert.equal(calculateBonuses("Premium", 50000), 0.1 * 2);
        done();
    });

    test('Valid program and boundary amount (100000)', (done) => {
        assert.equal(calculateBonuses("Standard", 100000), 2.5 * 0.05);
        done();
    });

    test('Invalid program and valid amount', () => {
        expect(calculateBonuses("Abcd", 25000)).toEqual(0);
      });

    test('Valid program and amount > 100000', () => {
        expect(calculateBonuses("Premium", 101000)).toEqual(0.25);
      });

})
